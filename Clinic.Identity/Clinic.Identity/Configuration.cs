﻿using IdentityServer4.Models;
using IdentityServer4;
using System;
using System.Collections.Generic;
using IdentityModel;
using System.Linq;
using System.Threading.Tasks;

namespace Clinic.Identity
{
    public static class Configuration
    {
        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("ClinicWebApi", "Web API")
            };
        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        public static IEnumerable<ApiResource> ApiResources =>
            new List<ApiResource>
            {
                new ApiResource("ClinicWebApi","Web API", new [] { JwtClaimTypes.Name})
                {
                    Scopes = { "ClinicWebAPI" }
                }
            };
        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "clinic-web-api",
                    ClientName = "Clinic Web",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RequirePkce = true,
                    RedirectUris =
                    {
                        "http://../signin-oidc"
                    },
                    AllowedCorsOrigins =
                    {
                        "http:// ..."
                    },
                    PostLogoutRedirectUris =
                    {
                        "http://.../signout-oidc"
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "ClinicWebAPI"
                    },
                    AllowAccessTokensViaBrowser = true
                }
            };
    }
}
