using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Clinic.Identity.Data;
using Clinic.Identity.Models;
using System.IO;
using Microsoft.Extensions.FileProviders;

namespace Clinic.Identity
{
    public class Startup
    {
        public IConfiguration AppConfiguration { get; set; }
        public  Startup(IConfiguration configuration)
        {
            AppConfiguration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string connection = AppConfiguration.GetConnectionString("DefaultConnection");
            services.AddDbContext<AuthDbContext>(options => options.UseNpgsql(connection));
            services.AddIdentity<AppUser, IdentityRole>(config =>
            {
                config.Password.RequiredLength = 4;
                config.Password.RequireDigit = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<AuthDbContext>()
              .AddDefaultTokenProviders();

            services.AddIdentityServer().AddInMemoryApiResources(Configuration.ApiResources)
                                        .AddAspNetIdentity<AppUser>()
                                        .AddInMemoryIdentityResources(Configuration.IdentityResources)
                                        .AddInMemoryApiScopes(Configuration.ApiScopes)
                                        .AddInMemoryClients(Configuration.Clients)
                                        .AddDeveloperSigningCredential();

            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = "Clinic.Identity.Cookie";
                config.LoginPath = "/Auth/Login";
                config.LogoutPath = "/Auth/Logout";

            });
            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "Styles")),
                RequestPath = "/styles"
            });
            
            app.UseRouting();
            app.UseIdentityServer();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
